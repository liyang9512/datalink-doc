---
home: true
heroImage: /img/logo.png
heroText: DATALINK
tagline: 🚀可视化数据处理编排
actionText: 开始使用 →
actionLink: /pages/start/info/#DATALINK
bannerBg: none # auto => 网格纹背景(有bodyBgImg时无背景)，默认 | none => 无 | '大图地址' | background: 自定义背景样式       提示：如发现文本颜色不适应你的背景时可以到palette.styl修改$bannerTextColor变量

features: # 可选的
  - title: 可视化规则编排
    details: 通过直观的拖拽和编排节点，轻松创建和管理数据流，简化数据集成和处理操作，大大提高数据集成和处理效率
  - title: 多种数据源支持
    details: 支持监听各类协议端口、订阅消息中间件和定时读取数据库，轻松满足不同的数据抓取需求，确保数据源的多样性和灵活性
  - title: 丰富的数据处理
    details: 提供丰富的数据处理节点，如分发、脚本函数处理等功能，满足复杂的数据处理需求，实现高效的数据转换和加工

# 文章列表显示方式: detailed 默认，显示详细版文章列表（包括作者、分类、标签、摘要、分页等）| simple => 显示简约版文章列表（仅标题和日期）| none 不显示文章列表
postList: none
---



