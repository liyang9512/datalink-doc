---
title: 快速开始
date: 2023年7月24日
permalink: /pages/start/fast/
article: false
---

## 环境要求

JDK1.8+

## 打包&运行

源码打包

```bash
git clone https://gitee.com/liyang9512/datalink.git
cd datalink
mvn -Prelease-datalink -Dmaven.test.skip=true clean install -U
```

安装包解压方式运行

```bash
unzip datalink-server-${version}.zip 或者 tar -xvf datalink-server-${version}.tar.gz
cd datalink/bin
```

Docker镜像方式运行 

```bash
docker pull leon9512/datalink:${version}
docker run -d --name datalink -p 9966:9966 leon9512/datalink:${version}
```

## 启动&停止

单节点模式：

```bash
cd datalink/bin

#windows start
startup.cmd

#linux start
sh startup.sh

#windows shutdown
shutdown.cmd

#linux shutdown
sh shutdown.sh
```

集群模式：（需在配置文件中配置节点列表）

```bash
cd datalink/bin

#windows start
startup.cmd -m cluster

#linux start
sh startup.sh -m cluster

#windows shutdown
shutdown.cmd

#linux shutdown
sh shutdown.sh
```

## Dashboard

程序启动后，使用浏览器访问服务部署容器的9966端口，即可打开管理页面。 \
默认用户名：admin 密码：datalink
![dashboard.png](../../.vuepress/public/img/doc/dashboard.png)

## 配置文件

主要配置说明：

```yaml
### 用于访问管理页面的端口,默认9966
server.port=9966

### 集群模式下节点列表配置,形式为 IP:端口,IP:端口,IP:端口
### 端口自定义且不可与server.port重复
### 第一组IP端口必须为本节点的IP端口
datalink.cluster.member.list=